package com.example.a2lesson5;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.format.DateUtils;
import android.util.Log;

public class DownloadService extends Service{
	
	@Override
	  public void onCreate() {
	    super.onCreate();
	    mockDownloadLargeFile();
	  }
	
	public void mockDownloadLargeFile() {
		Log.d("DownloadService", "Download Started");

		// Pause this thread for 15 seconds to simulate a long running process
		SystemClock.sleep(15 * DateUtils.SECOND_IN_MILLIS);

		Log.d("DownloadService", "Download Finished");
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
