package com.example.a2lesson5;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 startService(new Intent(MainActivity.this, DownloadService.class));

		    Handler handler = new Handler();
		    handler.postDelayed(new Runnable() {
		      @Override
		      public void run() {
		        stopService(new Intent(MainActivity.this, DownloadService.class));
		      }
		    }, 10000);
		  
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
